#pragma once

#include <cstdint>
#include <string>

#include "SDL.h"  //Squiqles because VSC can't find SDL.h, Bazel solves this.

class Graphics {
   public:
    Graphics();
    Graphics(const Graphics& g);
    ~Graphics();

    void draw();
    void updateCurrentSurface(int key);

   private:
    const int WIDTH = 640;
    const int HEIGHT = 480;

    SDL_Window* window = NULL;
    SDL_Surface* screenSurface = NULL;

    // TODO: make surface not represent a key
    static const int MAX_SURFACES = 5;
    SDL_Surface* surfaces[MAX_SURFACES];
    SDL_Surface* currentSurface = NULL;

    void initialize();
    void loadMedia();
    void cleanup();
    SDL_Surface* loadSurface(std::string path);
};
