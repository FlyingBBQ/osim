#pragma once

#include "gfx.hpp"

class Input {
   public:
    Input(Graphics* gfx);
    Input(const Input& input);
    ~Input();

    void processInput();  // Processes all queued input
    bool getQuitState();

   private:
    enum Keys {
        KEY_DEFAULT,
        KEY_UP,
        KEY_DOWN,
        KEY_LEFT,
        KEY_RIGHT,
        KEY_TOTAL
    };                  // Key press constants
    bool quit = false;  // If the users wants to quit or not
    Graphics* g;
};