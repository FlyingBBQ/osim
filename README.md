# osim
OS simulator - the game

## Dependencies
- Bazel
- C++ compiler

## Installing
TODO

## Building
Query all targets
`bazel query ...`

Build a target
`bazel build //src:osim`

Run a target
`bazel run //src:osim`

## Format

`./format.sh`
