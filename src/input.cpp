#include "input.hpp"

#include <SDL.h>

#include <iostream>

Input::Input(Graphics* gfx) : g(gfx) {}

Input::Input(const Input& input) { std::cout << __PRETTY_FUNCTION__ << std::endl; }

Input::~Input() { g = NULL; }

void Input::processInput() {
    int key = KEY_DEFAULT;  // Last input storage
    SDL_Event e;            // Event handler

    while (SDL_PollEvent(&e) != 0) {
        // User requests quit through 'X' button
        // If not polled, pressing 'X' button does nothing
        if (e.type == SDL_QUIT) {
            quit = true;
        }
        // Users presses a key
        else if (e.type == SDL_KEYDOWN) {
            switch (e.key.keysym.sym) {
                case SDLK_UP:
                    key = KEY_UP;
                    break;

                case SDLK_DOWN:
                    key = KEY_DOWN;
                    break;

                case SDLK_LEFT:
                    key = KEY_LEFT;
                    break;

                case SDLK_RIGHT:
                    key = KEY_RIGHT;
                    break;

                default:
                    key = KEY_DEFAULT;
                    break;
            }
            g->updateCurrentSurface(key);
        }
    }
}

bool Input::getQuitState() { return quit; }
