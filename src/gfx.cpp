#include "gfx.hpp"

#include <iostream>
#include <stdexcept>

Graphics::Graphics() {
    initialize();
    loadMedia();
}

Graphics::Graphics(const Graphics& g) { std::cout << __PRETTY_FUNCTION__ << std::endl; }

Graphics::~Graphics() { cleanup(); }

void Graphics::initialize() {
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) {
        throw std::runtime_error("Failed to init video/audio.");
    } else {
        // init window
        window = SDL_CreateWindow("hackermangs", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                                  WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
        if (window == NULL) {
            throw std::runtime_error("Failed to init window.");
        } else {
            screenSurface = SDL_GetWindowSurface(window);
        }
    }
}

void Graphics::loadMedia() {
    surfaces[0] = loadSurface("./assets/benissimo.bmp");
    surfaces[1] = loadSurface("./assets/soulja.bmp");
    surfaces[2] = loadSurface("./assets/mj.bmp");
    surfaces[3] = loadSurface("./assets/ppot.bmp");
    surfaces[4] = loadSurface("./assets/goblin.bmp");

    for (int i = 0; i < MAX_SURFACES; ++i) {
        if (surfaces[i] == NULL) {
            throw std::runtime_error("Could not load all media");
        }
    }
    currentSurface = surfaces[0];
}

void Graphics::draw() {
    // Position and size to be of image
    SDL_Rect stretch;
    stretch.x = 0;
    stretch.y = 0;
    stretch.w = WIDTH;
    stretch.h = HEIGHT;
    // Blit image to screen
    SDL_BlitScaled(currentSurface, NULL, screenSurface, &stretch);
    // Update window
    SDL_UpdateWindowSurface(window);
}

void Graphics::cleanup() {
    // Deallocate surface with image
    SDL_FreeSurface(screenSurface);
    screenSurface = NULL;

    // Destroy window
    // currentSurface is destroyed by this as well according to tutorial, so there is no leak
    SDL_DestroyWindow(window);
    window = NULL;

    SDL_Quit();
}

SDL_Surface* Graphics::loadSurface(std::string path) {
    // Final optimised image
    SDL_Surface* optimisedSurface = NULL;

    // Load image found at path
    SDL_Surface* tempSurface = SDL_LoadBMP(path.c_str());
    if (tempSurface == NULL) {
        std::cout << "Could not load image from given path, error: " << SDL_GetError() << '\n';
    } else {
        // Convert image to screen format
        // Conversurface returns a copy, why not assign the formatted image to screenSurface?
        optimisedSurface = SDL_ConvertSurface(tempSurface, screenSurface->format, 0);
        if (optimisedSurface == NULL) {
            std::cout << "Unable to optimize image, error: " << SDL_GetError() << '\n'
                      << path.c_str() << '\n';
        }
        // Free temporary surface
        SDL_FreeSurface(tempSurface);
        tempSurface = NULL;
    }
    return optimisedSurface;
}

void Graphics::updateCurrentSurface(int key) { currentSurface = surfaces[key]; }