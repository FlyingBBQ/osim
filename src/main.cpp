#include <iostream>

#include "gfx.hpp"
#include "input.hpp"

int main(void) {
    Graphics* gfx;
    Input* input;

    try {
        gfx = new Graphics();
        input = new Input(gfx);       
    } catch (const std::runtime_error& e) {
        std::cout << "Error: " << e.what() << std::endl;
    }
    
    if(gfx != NULL && input != NULL){
        while (!input->getQuitState()) {
            input->processInput();
            gfx->draw();
        }
    }

    return 0;
}